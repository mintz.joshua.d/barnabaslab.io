---
description: Personally, I think Vue is a great JavaScript framework. Here's why.
---
# Concerning Vue

Recently a coworker on another team asked me why I am so enthusiastic about Vue,
so I pointed him to [this well-written comparison on the Vue site.](https://vuejs.org/v2/guide/comparison.html)
He later clarified that he really wanted to know what I _personally_ enjoyed about working with Vue.

More than just dashing off a quick email reply, it's worth thinking about more in depth.
JavaScript developers can become opinionated about our preferred frameworks, and I'm no different.
Have I become biased? Are the reasons that I started using Vue still valid? Let's see.

## The Journey from AngularJS

I've been working on an internal content management application at Avalara for a few years.
I started the project with [AngularJS](https://angularjs.org/) because it was familiar and I was productive using it, 
along with the [Angular Material](https://material.angularjs.org/latest/) UI framework.
The Angular Material implementation of the Material Design spec provided an excellent, opinionated set of guidelines.

Two years into the project, however, things changed.
AngularJS and Angular Material were about to transition to [Long Term Support](https://docs.angularjs.org/misc/version-support-status).
Once Angular v2 was released, it became clear that that we'd have to rewrite or get left behind.
We needed to replace both Angular and Angular Material and gradually replace the existing application without disrupting our users.
Rather than simply plunge ahead with Angular, we took the opportunity to thoroughly evaluate the top frameworks.
It was important to choose wisely since we'd be living with the consequences for the next several years.
We narrowed down the contenders to:

* [Angular](https://angular.io/) + [Angular Material](https://material.angular.io/)
* [React](https://reactjs.org/) + [Material-UI](https://material-ui.com/)
* [Vue](https://vuejs.org/) + [Vuetify](https://vuetifyjs.com/en/)

I documented my findings for each experiment with pros and cons. For Vue specifically, I wrote:

#### Pros:
* Gentle learning curve, well-documented and generally satisfying to use
* The webpack template has good defaults including hot reloading and e2e testing with Nightwatch.js
* The single-file .vue components enabled by vue-loader loader make components clear
* Vuetify framework works well and has almost everything `$PROJECT_NAME` needs right away
* Less code needed overall

#### Cons:
* Vue is not as big as Angular or React yet, more wild-west
* Angular has Google, React has Facebook, but Vue has this one guy named Evan You
* Vuetify is not quite as rock solid as Angular Material just yet, but not deal-breaker level

I demoed the code to the other team members, and after we compared the total dev experience between the three frameworks, it was almost a total slam-dunk.

Since that time, Vue has not disappointed.
Although Angular and React were further ahead in terms of development experience at the time of that experiment, Vue has closed the gap with a CLI of its own.
I was wrong about Vue being "one guy", Vue is now supported by a well-funded team of dedicated developers.
More to our specific project needs, the Vuetify project has been absolutely top-notch.

## Personal Observations

### Vue vs. AngularJS

I didn't use Angular enough to draw major conclusions, other than that it was much more difficult to understand and use than Vue.
However, I spent years working with Angular's predecessor, AngularJS.
Perhaps because of that, I found Vue fit like a glove.
It felt like someone liked AngularJS so much that they decided to fix everything that was wrong with it and the result was Vue.
It appears like [that's exactly what happened:](https://vuejs.org/v2/guide/comparison.html#AngularJS-Angular-1)

> Some of Vue’s syntax will look very similar to AngularJS (e.g. `v-if` vs `ng-if`). 
This is because there were a lot of things that AngularJS got right and these were an inspiration for Vue very early in its development. 

I agree with and can add to that official Vue comparison page: 

* Vue has vastly improved runtime performance over AngularJS, I only wish we had measured it beforehand to quantify it
* AngularJS's two-way binding and watches caused many headaches; once we embraced computed properties, life quality improved

### Vue vs. React

As far as Vue vs. React for me personally, I enjoyed React, but I loved Vue.
If for our project, management had insisted or the team had voted to go with React, I would have been fine with it.
Comparing React and Vue isn't totally fair because they aren't really the same thing.

Compared to Angular or Ember, React is un-opinionated when it comes to things like routing and state management.
Many consider this aspect of React's design philosophy to be its main advantage, but personally I think Vue sits in the sweet spot of "opinionated-ness" between React and Angular.
The Vue framework comparison page puts it this way:

> Vue’s companion libraries for state management and routing (among other concerns) are all officially supported and kept up-to-date with the core library. 
React instead chooses to leave these concerns to the community, creating a more fragmented ecosystem.

This may be a matter of personal taste, but to me Vue single file components just look more elegant than React script + JSX.
There are a lot of lifecycle hooks in each framework, but (at the risk of sounding petty) Vue's naming conventions just make more sense to me.
Compare sample apps that do exactly the same thing, and React is more verbose and less expressive, in my eyes.
To me, these little things matter a lot.

### Anything vs. Anything

Honestly, I think that React vs. Angular vs. Vue matters less and less.
All of the mainstream modern JS libraries are converging on some best practices and they shamelessly borrow from each other.
It should take 2 minutes or less to create an app.
Reactivity is necessary magic.
Components are an obvious construct for web applications.
State management is a pit of despair and there should be obvious rails.
Tooling for debugging, unit testing, bundling and transpiling is a non-negotiable absolute requirement.
And so on.

I think the choice you make for a foundation for your application depends greatly on the state of the ecosystem at the moment you make your choice.
For this reason, it's important to evaluate alternatives at least annually.

## Further Reading

* [Comparison With Other Frameworks]((https://vuejs.org/v2/guide/comparison.html))
* [The State of JavaScript 2018 - Front-end Frameworks](https://2018.stateofjs.com/front-end-frameworks/overview/)
* [ReactJS vs Angular5 vs Vue.js — What to choose in 2018?](https://medium.com/@TechMagic/reactjs-vs-angular5-vs-vue-js-what-to-choose-in-2018-b91e028fa91d)
* [JS Web Frameworks Benchmark](https://stefankrause.net/js-frameworks-benchmark8/table.html)
* [Why I Left React for Vue](https://blog.sourcerer.io/why-you-should-leave-react-for-vue-and-never-use-it-again-5e274bef27c2)
* [Should You Use Vue Or React?](https://flaviocopes.com/vue-vs-react/)
* [State of Vue.js Report - 2019](https://www.monterail.com/state-of-vuejs-report)