---
description: This is a comparison of PostgreSQL DBaaS providers with an interactive calculator.
---
# PostgreSQL DBaaS Calculator

I recently wanted to start a side-project using the serverless application hosting service [Zeit Now](https://zeit.co/now).
Under "how do I use databases" in their FAQ, they recommend cloud databases such as Azure CosmosDB, MongoDB Atlas, Firebase, and Google Cloud SQL.
Personally, I want a relational database like PostgreSQL, and since it's 2019 there are lots of options.

While it's certainly _possible_ to self-host a PostgreSQL DB on a DigitalOcean droplet for $5/month, that doesn't mean it's a good idea.
In my opinion, outsourcing DB maintenance to a vendor is a no-brainer as long as your hosting bill is in the triple-digits.
Most side-projects will never get that far.

This page is an effort to compare DBaaS vendors for hosting a PostgreSQL database for my personal projects.
The focus is on these smaller side-project type deployments (\< $200/month or so), not giant enterprise-level clusters.
There are links to the pricing pages too (:moneybag:).
If you find an error or have a suggestion, please [open an issue](https://gitlab.com/barnabas/barnabas.gitlab.io/issues)
or better yet [submit a merge request](https://gitlab.com/barnabas/barnabas.gitlab.io/merge_requests).
Or, [leave a comment in this HN discussion](https://news.ycombinator.com/item?id=19534247).
Thanks!

## Vendor Overview

### [Amazon RDS for PostgreSQL](https://aws.amazon.com/rds/postgresql/)
I have personally used RDS for PostgreSQL at work and I have no complaints with it currently.
There are several instance types that have two vCPUs and vary by memory, but as soon as you need more CPU, you move to db.t3.xlarge.
Amazon's [T3 DB instance types use some kind of CPU bursting and CPU credit system](https://aws.amazon.com/rds/instance-types/), and yet DBaaS was supposed to be _easier_ somehow...
Amazon has significant discounts for reserved instances, so the calculator assumes a 1-year commitment with no upfront payment.
[:moneybag:](https://aws.amazon.com/rds/postgresql/pricing/)

### [Google Cloud SQL for PostgreSQL](https://cloud.google.com/sql/docs/postgres/)
Google's cloud offering is month-to-month, and they offer a discount for [sustained use](https://cloud.google.com/compute/docs/sustained-use-discounts#sud_table).
You are not tied to specific instance types; instead CPU, storage, and memory are all adjustable.
This makes a great deal of sense to me.
[:moneybag:](https://cloud.google.com/sql/docs/postgres/pricing)

### [Azure Database for PostgreSQL](https://azure.microsoft.com/en-us/services/postgresql/)
Azure does not break out their pricing for memory; you pick the machine class based on CPU and pay for storage.
Theoretically there are steep discounts for 1-year reservations, but I couldn't figure it out, so the calculator might be overestimating.
[:moneybag:](https://azure.microsoft.com/en-us/pricing/details/postgresql/)

### [Heroku Postgres](https://www.heroku.com/postgres)
Heroku is one of the few vendors with an OK free tier for Postgres, but I wouldn't count on it for hosting real things.
Unlike Azure, Heroku has pricing tiers based on memory, storage, and number of connections.
CPU is not visible or configurable, so the calculator below may not tell the whole story.
Surprisingly, there is a point at the 4 GB memory and 64 GB storage mark where Heroku is the best choice at $50/month.
Above that there's a big jump up to $200 that makes Heroku the most expensive option.
It seems like they're not really competing in the $100/month range.
[:moneybag:](https://www.heroku.com/pricing#databases)

### [DigitalOcean Managed Databases](https://www.digitalocean.com/products/managed-databases/)
DO is a newcomer to the PostgreSQL hosting world.
They are especially dominant in the 4-core and 6-core segment, costing about $60/month less than the next competitor, GCP.
[:moneybag:](https://www.digitalocean.com/pricing/#Databases)

### [ElephantSQL](https://www.elephantsql.com/)
ElephantSQL provides managed PostgreSQL hosting in a variety of other cloud platforms' data centers, including AWS, Softlayer, GCP, and Azure.
Here's another vendor that doesn't break out CPU and I wish they would at least mention it on the plan page.
As a plus, their service level plan names are downright adorable.
[:moneybag:](https://www.elephantsql.com/plans.html)

## Calculator
Here's a calculator to give a rough estimate of monthly costs for each major DBaaS vendor.
This does not factor in cost of network egress, backups or replicas; it's just for basic apples-to-apples between these services.

<ClientOnly>
  <dbaas-calculator />
</ClientOnly>

## Conclusions
*How Far Does That Dollar Go?*

For about **$100/month**:
* AWS: db.t3.large with 2 vCPU, 8 GB memory and 10 GB of storage
* GCP: also 2 CPU, 8 GB memory and 10 GB storage for $2 more than AWS
* Azure: B_Gen5_2 with 2 cores, 4 GB memory and *430* GB of storage

Heroku and DigitalOcean don't really compete at the $100 price point, but:
* For $50, Heroku offers "Standard 0" with 4 GB memory and 64 GB of storage
* For $60, DigitalOcean offers 4 GB memory, 2 vCPU, and 38 GB of storage

For about **$200/month**:
* AWS: db.t3.xlarge with 4 vCPU, 16 GB memory and 20 GB of storage
* GCP: also 4 CPU, 16 GB memory and 20 GB for $5 more than AWS
* Azure: GP_Gen5_2 with 2 cores, 10 GB memory is just $130, so you can blow the rest of storage again
* Heroku: Standard 2 is 8 GB memory and 256 GB of storage for exactly $200.
* DigitalOcean: 4 vCPU, 8 GB memory, 115 GB of storage for $120

GCP is flexible and competitive with the price for memory right now. I like it more now than ever.
There is a point where Heroku is the cheapest option, which blew me away. 
I just can't understand why there's a conspicuous hole in Heroku's lineup between Standard 0 and 2, except maybe customers who spend less than $200/month are 10x annoying?
Standard 1 must have had 6 GB memory and 128 GB storage for $100. That would have rocked.

### Further Reading
* [Pros and Cons of DBaaS](https://www.objectrocket.com/blog/company/advantages-and-disadvantages-of-dbaas/)
* [Comparing Cloud Database Options for PostgreSQL](https://severalnines.com/blog/comparing-cloud-database-options-postgresql)
