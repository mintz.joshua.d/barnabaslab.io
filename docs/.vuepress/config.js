module.exports = {
  title: 'Barnabas Kendall',
  description: 'My personal website',
  dest: 'public',
  plugins: [
    '@vuepress/last-updated',
    [
      '@vuepress/google-analytics',
      {ga: 'UA-131328476-1'}
    ]
  ],
  themeConfig: {
    nav: [
      {text: 'Articles', link: '/articles/'},
      {text: 'Projects', link: '/projects/'}
    ],
    sidebar: {
      '/articles/': ['postgres-dbaas', 'vue', 'webmaps']
    },
    repo: 'https://gitlab.com/barnabas/barnabas.gitlab.io',
    editLinks: true
  }
}