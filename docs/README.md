---
home: true
heroText: I am Barnabas Kendall.
heroImage: ./headshot.png
tagline: Welcome to my personal website.
actionText: My Résumé
actionLink: /cv.html
features:
- title: Creating
  details: As a software and UX engineer, I enjoy building things, especially web applications. I'll link to projects here.
- title: Writing
  details: Writing is one of my creative outlets, not a chore. Some of this is technical, some not.
- title: Learning
  details: Sharing the things I've learned here helps me to solidify it. If I've gotten something wrong, please get in touch!
footer: Copyright © 2018-now Barnabas Kendall
---

You can reach me:

- [LinkedIn](https://www.linkedin.com/in/barnabas/)
- [Twitter](https://twitter.com/BarnabasK)
- First dot last at gmail

