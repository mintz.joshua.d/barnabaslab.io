---
title: Résumé
navbar: false
---
# [Barnabas Kendall](/)

I enjoy solving a particular kind of business problem using hard-fought technical experience and intuition. 
I know how to listen carefully and learn quickly. 
I understand how complex applications are made, how they break, and how to fix them.

Tech that I have used includes: JavaScript (Node.js, Vue, AngularJS, jQuery) .NET (ASP.NET/C#), Demandware, PHP (CodeIgniter, Kohana, CakePHP), Java, Python (Django, Pyramid, GAE); database design and administration (Postgres/PostGIS, SQL Server, Oracle, MySQL).

I have done full life cycle project planning, resource management, budgeting, virtual teams/offshore, training and mentoring. 
I've worked in a variety of industries such as tax, recurring revenue analytics, consulting, mobile, eCommerce, web search, energy, mortgage, pharmaceutical, and casual games.

 
### Avalara
#### Senior Software Engineer, UX
- Mar 2015 – Present
- Seattle, Washington / Remote

I'm the technical and UX lead on an internal tool for managing tax content. The product is built with Node, Express, Postgres/PostGIS, and Vue. 
I've been at the forefront of the product since inception. 
I've done user needs analysis, storyboarding, wireframe mockups, static prototypes, documentation, training, and videos. 
I architected a typical web application with a static front end built with Angular/Webpack/Gulp and dynamic backend API originally in ASP.NET, later in Node.js/Express. 
I integrated the product with Okta to enable SSO for our internal user base. 
I implemented a geographic boundary editor. 
In 2018, I proposed and led the successful implementation of a gradual rewrite from Angular to Vue. 
As a team lead, I spend a significant amount of time in code reviews and other training.

### Scout by ServiceSource
#### Senior Web Application Developer
- Sep 2012 – Mar 2015
- Issaquah, Washington

I was the team lead of three web developers, and I worked closely with the product manager to design and implement new features, enhancements, and bug fixes to our customer-facing web application and some internal web applications.
- Responsible for developing the company’s SaaS web application, “Optimizer”
- Responsible for maintaining the internal management web UI “Operations”
- Contributed to maintenance of company's legacy MLS product
- Spearheaded rewrite of Optimizer and Operations over from prototype to production using ASP.NET MVC4, Web API, AngularJS and other technologies
- Worked on a project to re-contextualize Optimizer as a Salesforce canvas app, including Salesforce OAuth for SSO with our own internal authentication


### Live Area Labs
#### eCommerce Developer
- Apr 2012 – Sep 2012
- Seattle, Washington

I developed and managed the user experience and administrative sections of several large eCommerce stores (including Fila, Brooks, LUSH, Urban Decay), as well as other projects using various CMS. 
Working with a talented graphic design and branding team, I translated their art into function using responsive web technologies. 
I often supplemented an on-site development team as an advisor and technical architect, or solved eCommerce outages or production issues.
I was able to contribute to implementing coding, issue tracking, and source control procedures, specifically by introducing and managing the team's transition to Assembla. 
I also spearheaded a developer brown-bag lunch to discuss technology topics.


### Blue Nile
#### Website Developer
- Aug 2011 – Apr 2012
- Seattle, Washington

As part of site development team, performed both routine site updates and designed new user-experience. Large projects include an interactive charm designer (heavy jQuery UI + AJAX) and a 360-degree ring viewer.

### Independent Consultant
- Jan 2008 – Aug 2011
- Aliso Viejo, California

I helped a variety of companies with public and private web projects such as:
- Open source project for Twilio.com (not yet launched) using the Twilio API, CodeIgniter, and complex JQuery Javascript.
- ThisLineIsSecure.com - Mashup between Twilio's API and Google Checkout
- InMyPocket.com - Kohana/PHP website on Mosso (now Rackspace) multiplayer game using SmartFox server and interfacing with TwoFish game e-commerce service
- (mortgage help site) Microsoft InfoPath Forms Services + SharePoint


### Greenlight Wireless
#### CTO / Lead Developer
- Aug 2004 – Dec 2007
- Lake Forest, California

I built and managed the hardware and software infrastructure of Greenlight Wireless, a small startup.

### Commerce Velocity
#### Application Architect
- 2001 - 2003
- Irvine, California

### Software Architects
#### Senior Consultant
- 2000 - 2001
- Houston, Texas

### AltaVista Shopping.com
#### Software Developer
- 1998 - 2000
- Newport Beach, California